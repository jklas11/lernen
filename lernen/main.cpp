// ask for someones name and generate a framed greeting

#include <iostream>
#include <string>
#include <iomanip>
#include <ios>
#include <algorithm>
#include <vector>
#include <stdexcept>

using std::cin;             using std::endl;
using std::cout;            using std::string;
using std::streamsize;      using std::setprecision;
using std::vector;          using std::sort;
using std::istream;
using std::domain_error;

double median(vector<double> vec){
    typedef vector<double>::size_type vec_sz;

    vec_sz size = vec.size();
    if(size==0){
        throw domain_error("median of an empty vector");
    }
    sort(vec.begin(), vec.end());
    vec_sz mid = size/2;

    return size % 2 == 0 ? (vec[mid] + vec[mid-1]) / 2 : vec[mid];
}

double grade(double midterm, double final, double homework) {

    return 0.2*midterm+0.4*final+0.4*homework;
}

double grade(double midterm, double final, const vector<double>& hw){

    if (hw.size() == 0)
        throw domain_error("student has done no homework");
    return grade( midterm, final,median(hw) );
}


//read homework grades form an input steram into a vector<double>& hw
istream& read_hw(istream& in, vector<double>& hw){
    if(in){
        //get rid of preious content
        hw.clear();

        //read homework grades
        double x;
        while (in >> x){

                hw.push_back(x);}

        in.clear();
    }
    return in;
}


int main() {
    cout << "Please enter your first name \n\n";
    std::string name;
    std::cin >> name;

    //build the message that we intend to write
    const std::string greeting = "Hello, " + name + "!";

    const int pad = 1;

    const int rows = pad*2+3;
    const string::size_type cols = greeting.size() + pad * 2 + 2;

    cout << endl;

    for (int r = 0; r != rows; ++r) {

        string::size_type c = 0;

        while (c != cols){
        // is it time to write the greeting?
            if ( r == pad + 1 && c == pad + 1){
                cout << greeting;
                c += greeting.size();
            }else{
                if ( r == 0 || r == rows -1 || c == 0 || c == cols -1 )
                    cout << "*";
                else
                    cout << " ";
                ++c;
            }
        }

    cout << endl;
    }

    // ask for and read the midterm and final grades
    cout << "Please enter your midterm and final exam grades: ";
    double midterm, final;
    cin >> midterm >> final;

    // ask for the homework grades
    cout << "Enter all your homework grades, "
            "followed by end-of-file: ";

    vector<double> homework;

    //read the homework grades
    read_hw(cin,homework);

    //compute and generate the final grade, if possible
    try {
        double final_grade = grade(midterm, final, homework);
        streamsize prec = cout.precision();
        cout << "Your final grade is " << setprecision(3)
             << final_grade << setprecision(prec) << endl;
    } catch (domain_error) {
        cout << endl << "you must enter your grades.   " "please try again." << endl;
        return 1;
    }

    return 0;
}
